### Tutor 13

> Jekyll Plugin with Ruby

* Filter: Year Text, Term Array, Pagination Links, Link Offset Flag, Keywords

* Generator: Tag Names

![Jekyll Bulma: Tutor 13][jekyll-bulma-preview-13]

-- -- --

What do you think ?

[jekyll-bulma-preview-13]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-13/jekyll-bulma-md-preview.png
