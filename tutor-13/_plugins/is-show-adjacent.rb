# Pagination links 
# https://glennmccomb.com/articles/how-to-build-custom-hugo-pagination/

module Jekyll
  module IsShowAdjacent
    def is_show_adjacent(cursor, current, total_pages, link_offset)

      # initialize show cursor flag
      flag = false

      # link_offset related variables
      max_links   = (link_offset * 2) + 1;
      lower_limit = 1 + link_offset;
      upper_limit = total_pages - link_offset;

      if total_pages > max_links
        # Complex page numbers
        case
        when current <= lower_limit
          # Lower limit pages.
          # If the user is on a page which is in the lower limit.
          flag = true if cursor <= max_links
        when current >= upper_limit
          # Upper limit pages.
          # If the user is on a page which is in the upper limit.
          flag = true if cursor > (total_pages - max_links)
        else
          # Middle pages.
          if ( (cursor >= current - link_offset) &&
               (cursor <= current + link_offset) )
            flag = true 
          end
        end
      else
        # Simple page numbers.
        flag = true
      end

      flag
    end
  end
end

Liquid::Template.register_filter(Jekyll::IsShowAdjacent)
