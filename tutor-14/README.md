### Tutor 14

> Jekyll Theme with Assets and SASS

* Bundling Gem

* Using Theme

![Jekyll Bulma: Tutor 14][jekyll-bulma-preview-14]

-- -- --

What do you think ?

[jekyll-bulma-preview-14]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-14/jekyll-bulma-md-preview.png
