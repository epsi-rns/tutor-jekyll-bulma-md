Gem::Specification.new do |spec|
  spec.name        = 'oriclone-bulma-md'
  spec.version     = '0.2.0'
  spec.date        = '2020-07-11'
  spec.authors     = ["E.R. Nurwijayadi"]
  spec.email       = 'epsi.nurwijayadi@gmail.com'

  spec.summary     = "Oriclone Bulma MD - Jekyll Theme"
  spec.description = "An example of Jekyll Theme with Bulma Material Design"
  spec.homepage    = "https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md"
  spec.license     = "MIT"

  require 'rake'
  spec.files       = Dir.glob("{_layouts,_includes,_sass,assets}/**/**/*") +
                     ['LICENSE']

  spec.add_runtime_dependency 'jekyll-paginate-v2', '~> 3.0', '>= 3.0.0'
end
