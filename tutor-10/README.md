### Tutor 10

> Loop with Liquid

* More Content: Quotes. Need this content for demo

* Simplified All Layout Using Liquid Template Inheritance

* Article Index: By Year, List Tree (By Year and Month)

![Jekyll Bulma: Tutor 10][jekyll-bulma-preview-10]

-- -- --

What do you think ?

[jekyll-bulma-preview-10]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-10/jekyll-bulma-md-preview.png
