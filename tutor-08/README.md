### Tutor 08

> Add Bulma CSS Framework

* Add Bulma CSS

* Standard Header (jquery or vue or native) and Footer

* Enhance All Layouts with Bulma CSS

* Apply Bulma Two Column Responsive Layout for Most Layout

![Jekyll Bulma: Tutor 08][jekyll-bulma-preview-08]

-- -- --

What do you think ?

[jekyll-bulma-preview-08]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-08/jekyll-bulma-md-preview.png
