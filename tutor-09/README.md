### Tutor 09

> Combine with Custom SASS Material Design

* Add (a bunch of) Custom SASS (Custom Design)

* Nice Header and Footer

* Enhance All Two Column Responsive Layout with Material Design

* Nice Tag Badge in Tags Page

![Jekyll Bulma: Tutor 09][jekyll-bulma-preview-09]

-- -- --

What do you think ?

[jekyll-bulma-preview-09]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-09/jekyll-bulma-md-preview.png
