### Tutor 11

> Features

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Refactoring Template using Capture: Widget

* Pagination (v1): Adjacent, Indicator, Responsive

* Pagination (v2): Adjacent, Indicator, Responsive

* Post: Header, Footer, Navigation

![Jekyll Bulma: Tutor 11][jekyll-bulma-preview-11]

-- -- --

What do you think ?

[jekyll-bulma-preview-11]:  https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/raw/master/tutor-11/jekyll-bulma-md-preview.png
