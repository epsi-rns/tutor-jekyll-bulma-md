---
layout      : post
title       : Julien Baker - Something
date        : 2018-09-07 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
keywords    : [sad, emo, broken]

author      : Julien Baker
toc         : toc/2018-09-julien-baker.html

related_link_ids :
  - 18091335  # Sprained Ankle
---

I knew I was wasting my time.  
Keep myself awake at night.  
'Cause whenever I close my eyes.  
I'm chasing your tail lights.  
In the dark.  
In the dark.  
In the dark.

Watched you said nothing, said nothing, said nothing.  
I can't think of anyone, anyone else.  
I can't think of anyone, anyone else.  
I can't think of anyone, anyone else.  
I won't think of anyone else.
